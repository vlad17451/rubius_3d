import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import PortalVue from 'portal-vue'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(PortalVue)

Vue.config.productionTip = false

// const token = ''
//
// Vue.prototype.$http = axios
//
// Vue.prototype.$http.defaults.headers.common['auth'] = token

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
