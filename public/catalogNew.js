export const catalog = async () => {
    let name
    let cost
    let idList
    let promiseName
    let promiseId
    let promiseCost
    let point
    const points = [
        'glass',
        'cloth',
        'veneer',
        'plasticMain',
        'plasticTable',
        'chair',
        'doorlock',
        'facing',
        'interior',
        'table',
        'speak',
        'smartglass',
        'charger'
    ]
    let catalog = {
        glass: [],
        cloth: [],
        veneer: [],
        plasticMain: [],
        plasticTable: [],
        chair: [],
        doorlock: [],
        facing: [],
        interior: [],
        table: [],
        speak: [],
        smartglass: [],
        charger: []
    }

    if (navigator.languages[0].substring(0, 2) === 'ru') {
        promiseName = fetch('./namesRu.json')
            .then(response => response.json())
            .then((response) => {
                name = response
            })
    } else {
        promiseName = fetch('./namesEn.json')
            .then(response => response.json())
            .then((response) => {
                name = response
            })
    }
    console.log('Currennt language:', navigator.languages[0].substring(0, 2))
    console.log('Languages of agent (from navigator):', navigator.languages)
    promiseCost = fetch('./costNew.json')
        .then(response => response.json())
        .then((response) => {
            cost = response
        })
    promiseId = fetch('./idList.json')
        .then(response => response.json())
        .then((response) => {
            idList = response
        })
    await Promise.all([promiseName, promiseCost, promiseId])

    points.forEach(item => {
        point = item
        idList[point].forEach((item, index) => {
            catalog[point].push(Object.assign(
                idList[point][index],
                cost[point][index],
                name[point][index])
            )
        })
    })
    return {
        catalog,
        points,
        general: {
            mainCost: cost.mainCost,
            local: name.default
        }
    }
}
